/*

App: E-Commerce API

	Scenario:
		An  E-Commerce system application where a user can order a product.

	Type: E-Commerce API (Web App)
	
	Description:

		An E-Commerce system application system application where a user can view/order a product
		Allows an admin to do CRUD operations
		Allows users to register into our database

	Features:

        User registration
        User authentication
        Set user as admin (Admin only)
        Retrieve all active products
        Retrieve single product
        Retrieve all products
        Create Product (Admin only)
        Update Product information (Admin only)
        Archive Product (Admin only)
        Make Product Available (Admin only)
        Non-admin User checkout (Create Order)
        Retrieve authenticated user’s orders
        Retrieve all orders (Admin only)
        Retrieve User Details
        Check if Email Exists


    Other Features:

        Add to Cart
        Added Products
        Change product quantities
        Remove products from cart
        Subtotal for each item
        Total price for all items


    Data Model:

        User
        firstName - string,
        lastName - string,
        email - string,
        password - string,
        mobileNo - string,
        isAdmin - boolean,
                default: false


        Product
        name - string,
        description - string,
        price - number
        isActive - boolean
                default: true,
        createdOn - date
                    default: new Date()
        orders - [
            {
                orderId - string,
                quantity - number

            }
        ]


        Order
        userId - string
        totalAmount - number,
        purchasedOn - date
                    default: new Date(),
        products - [

            {
                productId - string,
                quantity - number
            }

        ]


        */