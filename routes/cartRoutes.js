const router = require('express').Router();
const auth = require('../auth');
const cartController = require('../controllers/cartControllers');


router.post("/create",(req,res)=>{

    cartController.createCart(req.body).then(createCart=>res.send(createCart));
})

router.get("/",auth.verify,(req,res)=>{

    const userId = auth.decode(req.headers.authorization).id;

    cartController.getCart(userId).then(userCart=>res.send(userCart));
})

router.get("/usercart",auth.verify,(req,res)=>{

    const userId = auth.decode(req.headers.authorization).id;

    cartController.getUserCart(userId).then(userCart=>res.send(userCart));
})





router.patch("/:productId/updateQuantity",auth.verify,(req,res)=>{

    const data = {

        userId: auth.decode(req.headers.authorization).id,
        productId:req.params.productId,
        quantity: req.body.quantity,
     
    }

    cartController.updateQuantity(data).then(updatedQuantity=>res.send(updatedQuantity));
})

router.delete("/:productId/deleteCartItem",auth.verify,(req,res)=>{

     const data = {

        userId: auth.decode(req.headers.authorization).id,
        productId:req.params.productId
     
    }

    cartController.removeProduct(data).then(removedProduct=>res.send(removedProduct));

})
router.delete("/delete",auth.verify,(req,res)=>{

     const data = {

        userId: auth.decode(req.headers.authorization).id,
        
     
    }

    cartController.removeProduct(data).then(removedProduct=>res.send(removedProduct));

})


module.exports = router;