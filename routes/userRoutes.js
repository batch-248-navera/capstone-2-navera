const router = require('express').Router();
const auth = require('../auth')

const userController = require("../controllers/userControllers");
const orderController = require("../controllers/orderControllers");


router.post("/checkEmail",(req,res)=>{

    userController.checkEmailExist(req.body).then(result=>res.send(result));

})


router.post("/register",(req,res)=>{

    userController.register(req.body).then(registered=>res.send(registered));

})


router.post("/login",(req,res)=>{

    userController.loginUser(req.body).then(login=>res.send(login));

})

router.get("/all",(req,res)=>{

    userController.getAllUsers().then(allUsers =>res.send(allUsers));
})


router.patch("/assignAdmin/:userId", auth.verify, (req,res)=>{

    const data = {
        userId: req.params.userId,
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }

    userController.assignAdmin(data).then(assignedAdmin=>res.send(assignedAdmin));

})

router.post("/checkout",auth.verify,(req,res)=>{

    const data ={
        userId:auth.decode(req.headers.authorization).id,
        isAdmin:auth.decode(req.headers.authorization).isAdmin,
        reqBody:req.body
    } 

    orderController.createOrder(data).then(order=>res.send(order));
})

router.post("/userOrders",auth.verify,(req,res)=>{
    
    const data = {

        userId:auth.decode(req.headers.authorization).id,
        isAdmin:auth.decode(req.headers.authorization).isAdmin,
        reqBody:{
            products:[
                {
                productId: req.body.productId,
                quantity: req.body.quantity
                     }
                ],
            
            totalAmount:req.body.totalAmount
            }
        
    }

    orderController.createOrder(data).then(order=>res.send(order));

})

router.get("/allOrders",auth.verify,(req,res)=>{

    const userId = auth.decode(req.headers.authorization).id;
    
    
    orderController.getUserOrders(userId).then(order=>res.send(order));
})

router.get("/details",auth.verify,(req,res)=>{

    const userId = auth.decode(req.headers.authorization).id;
    
    
    userController.getUserDetails(userId).then(userDetails=>res.send(userDetails));
})



module.exports = router;