const router = require('express').Router();
const auth = require("../auth");
const productController = require("../controllers/productControllers");
const cartController = require('../controllers/cartControllers');


router.post("/add",auth.verify,(req,res)=>{
     
    const data = {
        
        product : req.body,
        isAdmin : auth.decode(req.headers.authorization).isAdmin
	}
    
    productController.addProduct(data).then(addedProduct =>res.send(addedProduct));
})

router.get("/active",(req,res)=>{
    
    productController.getActiveProducts().then(activeProducts =>res.send(activeProducts));
})

router.get("/all",(req,res)=>{

    productController.getAllProducts().then(allProducts =>res.send(allProducts));
})

router.get("/:productId",(req,res)=>{

    productController.getProductById(req.params.productId).then(product =>res.send(product));
    
})


router.patch("/update/:productId",auth.verify,(req,res)=>{

    const data = {
        
        productId: req.params.productId,
        product: req.body,
        isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

    productController.updateProduct(data).then(updatedProduct=>res.send(updatedProduct));

})

router.patch("/:productId/archive",auth.verify,(req,res)=>{

    const data = {
        
        productId : req.params.productId,
        isAdmin : auth.decode(req.headers.authorization).isAdmin
	}

    productController.archiveProduct(data).then(archivedProduct=>res.send(archivedProduct));
})

router.patch("/:productId/unarchive",auth.verify,(req,res)=>{

    const data = {
        
        productId : req.params.productId,
        isAdmin : auth.decode(req.headers.authorization).isAdmin
	}
    

    productController.unarchiveProduct(data).then(unarchivedProduct=>res.send(unarchivedProduct));
})

router.post("/:productId/addToCart",auth.verify,(req,res)=>{

    const data = {

        userId: auth.decode(req.headers.authorization).id,
        quantity: req.body.quantity,
        productId: req.params.productId

    }

    cartController.addToCart(data).then(addedToCart=>res.send(addedToCart));
})

router.delete("/:productId/delete",auth.verify,(req,res)=>{

    const data = {
        
        productId : req.params.productId,
        isAdmin : auth.decode(req.headers.authorization).isAdmin
	}
    

    productController.deleteProduct(data).then(result=>res.send(result));
})

module.exports = router;