const router = require('express').Router();
const auth = require('../auth');
const orderController = require('../controllers/orderControllers');


router.post("/",auth.verify,(req,res)=>{
    const data ={
        isAdmin:auth.decode(req.headers.authorization).isAdmin,
        userId:auth.decode(req.headers.authorization).id,
        reqBody:req.body
    } 

    orderController.createOrder(data).then(order=>res.send(order));
})

router.get("/all",auth.verify,(req,res)=>{
    
    const isAdmin = auth.decode(req.headers.authorization).isAdmin
	
    orderController.getAllOrders(isAdmin).then(allOrders => res.send(allOrders))
})





module.exports = router;