require('dotenv').config();

const jwt = require("jsonwebtoken");
 

module.exports.createAccessToken = (user) =>{

    const data = {
        id: user.id,
        email: user.email,
        isAdmin: user.isAdmin
    }

    return jwt.sign(data, process.env.ACCESS_TOKEN_SECRET,{});

}

module.exports.verify = (req,res,next) => {

    let token = req.headers.authorization;

    if( token !== undefined){

       token = token.split(' ')[1];
   
        return jwt.verify(token,process.env.ACCESS_TOKEN_SECRET,(err,data)=>{

            if(err){
                return res.status(403).send({message: 'Auth failed! Invalid token.'});
            }

            else{
                next();
            }

        })
       
    }

    else{

        return res.status(403).send({auth:"failed", message: 'No token provided.'});
    }

}

module.exports.decode = (token) => {

    
	if( token !== undefined){

        token = token.split(' ')[1];

		return jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, data) => {

		
				return err?null:jwt.decode(token, {complete:true}).payload;
			
		})


	} else {

		return res.status(403).send({auth:"failed", message: 'No token provided.'});

	}

}
