const bcrypt = require("bcrypt");
const auth = require("../auth");
const User = require("../models/User");
const cartController = require('../controllers/cartControllers');


module.exports.register = async (reqBody) =>{


        let newUser = new User({
            username: reqBody.username,
            firstName: reqBody.firstName,
            lastName: reqBody.lastName,
            email: reqBody.email,
            gender: reqBody.gender,
            bday: reqBody.bday,
            mobileNo: reqBody.mobileNo,
            password: bcrypt.hashSync(reqBody.password, 10)
        })
    

    return newUser.save().then((user, error)=>{

        if(error){
            return false;
        }

        else{
        
            return user;
        
        }

    })
}   
    


module.exports.checkEmailExist = (reqBody) =>{

    return User.find({email: reqBody.email}).then(result=>result.length>0?true:false);
}

module.exports.loginUser = (reqBody)=>{

    return User.findOne({email: reqBody.email}).then(user=>{

        if(!user){
            return false;
        }
        else{
            
            return bcrypt.compareSync(reqBody.password, user.password)?{access: auth.createAccessToken(user)}:false;
            
        }
        
     });
}

module.exports.getAllUsers = () =>{

    return User.find({}).then((allUsers,error)=>{

       if(error){
            return false;
       } 

       else{
         return allUsers;
       }

        
    });

}

module.exports.assignAdmin = ({userId,isAdmin})=>{

        if(isAdmin){

            const newUserRole = {
                isAdmin : true
            }

            return User.findByIdAndUpdate(userId,newUserRole).then((newAdmin,error)=>{
                    
                    return error?false:{message: "User role changed to Admin"};

                    });
        }
        else{
            return {message: "Login to your admin account to make this changes."};
        }

}

module.exports.getUserDetails = (userId) =>{
        
        return User.findById(userId).then((user,error)=>{

            if(error){
                return false;
            }

            else{
                return user;
            }

        });
}
