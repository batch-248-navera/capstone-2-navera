const Order = require("../models/Order");
const Product = require("../models/Product");



module.exports.createOrder = async(data)=>{

       if(!data.isAdmin){

        const newOrder = new Order({

            userId : data.userId,
            products: data.reqBody.products,
            totalAmount: data.reqBody.totalAmount,

        });



        const updateOrder =  await newOrder.save().then((order,error)=>{

                if(error){
                    return false;
                }
                else{
                    return order;
                }

             });

     
        let updateProduct;

        updateOrder.products.forEach(product=>{
           
            updateProduct = Product.findById(product.productId).then(orderProduct=>{

      
                    orderProduct.orders.push({orderId: product.id,quantity: product.quantity});

                    return orderProduct.save().then((order,error)=>{

                                if(error){
                                    return false;
                                }
                                else{
                                    return true;
                                }
                            
                            })         
                       })
                 })

    return updateProduct?{messaage: "Order placed succesfully."}:false;

    }
    else{
        return {Message: "Login to your non-Admin account."}
    }



         
    module.exports.getAllOrders =  (isAdmin) =>{

        return isAdmin? Order.find({}).then((allOrders,error)=>{
    
            if(error){
                return false;
            }
            else{
    
                return allOrders;
    
            }
        }):false;
    
    }
    
}


         
    


module.exports.getAllOrders =  (isAdmin) =>{

    return isAdmin? Order.find({}).then((allOrders,error)=>{

        if(error){
            return false;
        }
        else{

            return allOrders;

        }
    }):false;

}

module.exports.findUserOrders = (UserId) =>{
  
    return Order.findById(UserId).then((findUserOrder,error)=>{
    
    if(error){

        return false;
    }
    else{

        return findUserOrder;

    }
});

}