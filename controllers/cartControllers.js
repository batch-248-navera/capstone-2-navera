const Cart = require('../models/Cart');
const Product = require('../models/Product');




const totalAmount = (userCart) =>{

    let total = 0;

    userCart.products.forEach(product => {
        
          total = total + product.subTotal;
         
    });

    return total;
     
}

module.exports.createCart = async (ReqBody) =>{


    let newCart = new Cart({
        userId: ReqBody.userId
    })

try{
    return newCart.save().then((user, error)=>error?false:true);
}

catch(error){
    return error;
}

}  

module.exports.addToCart = async(data) =>{
    
    const item = await Product.findById(data.productId);
    const userCart = await Cart.findOne({userId: data.userId});
   

        if(userCart){
            
          
         let productItem;

         userCart.products.forEach(product => {
                
              if(product.productId === data.productId){
                
                productItem = product ;

              }
           
          })             
          
          
          if(productItem){
                productItem.quantity += 1;
                productItem.subTotal = productItem.price * productItem.quantity;
                userCart.totalPrice = totalAmount(userCart);

                return userCart.save().then((productItem,error)=>{
                    return error?false:{message:"Product added to cart."};
                });
          }

          else{

                const newItem = {
            
                    productId: data.productId,
                    name: item.name,
                    quantity: 1,
                    price: item.price,
                    subTotal: item.price * 1
            
            }

            
                 userCart.products.push(newItem);
                 userCart.totalPrice = totalAmount(userCart) + item.price;


                return userCart.save().then((newItem,error)=>{
                    return error?false:{message:"Product added to cart."};
            
                });
            
        
          }
          
                   
        }
        else{

            
            const newCartItem = new Cart({

                userId: data.userId,
                products:[{
                    productId: data.productId,
                    name: item.name,
                    quantity: 1,
                    price: item.price,
                    subTotal: item.price 
                    
                }],
                totalPrice: item.price
                
                
            }) 
            
            
            
            return newCartItem.save().then((newItem,error)=>{
                 return error?false:{message:"Product added to cart."};
             });

        }
    
    }

   
module.exports.getCart = (userId) => {
    
    
    return Cart.findOne({userId: userId}).then((userCart,error)=>{

        return error?false:userCart.products;
    });
    
  
}
module.exports.getUserCart = (userId) => {
    
    
    return Cart.findOne({userId: userId}).then((userCart,error)=>{

        return error?false:userCart;
    });
    
  
}
    


module.exports.updateQuantity = async(data) =>{
    
    const userCart = await Cart.findOne({userId: data.userId});
   
    let quantityUpdated;

    userCart.products.forEach(product=>{

        if(product.productId === data.productId){ 
            
            product.quantity = data.quantity
            product.subTotal = product.quantity * product.price; 
            userCart.totalPrice = totalAmount(userCart);

             quantityUpdated = userCart.save().then((updatedQuantity,error)=>{

                return error?false:true;
             
            });
        }    
    
    })

    return quantityUpdated?{message: "Item quantity updated."}:false;

 }   

    module.exports.removeProduct = async(data) =>{
    
        const userCart = await Cart.findOne({userId: data.userId});
        
        if(userCart){
            
        let itemRemoved;

        userCart.products.forEach(item=>{
           
            if(item.productId === data.productId){
            
                userCart.products.pull(item.id)


                userCart.totalPrice = totalAmount(userCart);
                itemRemoved = userCart.save().then((updatedQuantity,error)=>{
            
                    return error?false:true;
            
                });
            }

            
        });
        
     
        return itemRemoved?{message:"Cart item was removed successfully."}:false;
    }
    else{

        return {message: "Product not found."}
    }

        
    

    }

    module.exports.removeProduct = async(data) =>{
    
        const userCart = await Cart.findOne({userId: data.userId});
       
        try{
            if(userCart){
            
         userCart.totalPrice =0;
         userCart.products =[];

         userCart.save().then((result,error)=>{

            return error?false:true;
         
        })
        
        }
        else{
            return false;
               }  
            
       }
    
    catch(error){
            return error;
        }
        
    }

