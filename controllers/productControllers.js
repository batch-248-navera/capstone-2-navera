
const Product = require("../models/Product");


module.exports.addProduct = ({product,isAdmin}) =>{

    if(isAdmin){

        let newProduct = new Product({

            image: product.image,
            name: product.name,
            description: product.description,
            brand: product.brand,
            category: product.category,
            price: product.price,
            quantity: product.quantity
        })

        return newProduct.save().then((product,error)=>error?false:{message:"Product added successfully."});
        
    }

    else{
        return false;
    }

    
}

module.exports.getActiveProducts = () =>{

    return Product.find({isActive: true}).then(activeProducts=>activeProducts);
}

module.exports.getAllProducts = () =>{

    return Product.find({}).then(allProducts=>allProducts);

}

module.exports.getProductById = (productId) =>{
    
    return Product.findById(productId).then(product=>product);

}

module.exports.updateProduct = (data) =>{

try{
    if(data.isAdmin){

        let updateProduct = {
            name: data.product.name,
            description: data.product.description,
            price: data.product.price
        }

        return Product.findByIdAndUpdate(data.productId,updateProduct).then((updateProduct, error)=>{

            if(error){
                return false;
            }
            else{
                
                return true;
            }
        });

    }
    else{
        return false;
    }
}

catch (error) {
    return (error);
}
    
}

module.exports.deleteProduct = (data) =>{

try{
    if(data.isAdmin){

        
        return Product.findByIdAndRemove(data.productId).then((result, error)=>{

            if(error){
                return false;
            }
            else{
                
                return true;
            }
        });

    }
    else{
        return false;
    }
}

catch (error) {
    return (error);
}
    
}

module.exports.archiveProduct =  ({productId,isAdmin})=>{

try{
    if(isAdmin){

    
        return Product.findByIdAndUpdate({_id:productId},{isActive: false}).then((archivedProduct,error)=>{

            if(error){
                return false;
            }
            else{
                
                
                return true;
            }
        });
        
    }
    else{
       return false; 
    }
  }  
  catch (error) {
    return (error);
}

}


module.exports.unarchiveProduct = ({productId,isAdmin})=>{

    if(isAdmin){


        return Product.findByIdAndUpdate({_id:productId},{isActive: true}).then((unarchivedProduct,error)=>{

            if(error){
                return false;
            }
            else{
                
                
                return {message: "Product unarchived successfully."};
            }
        });

    }
    else{
       return false; 
    }
    
}

