const mongoose = require('mongoose');

const cartItems = new mongoose.Schema({
    
    productId:{
        type:String
    },
    image:{
        type: String
    },
    name:{
        type:String
    },
    quantity:{
        type:Number,
        default:0
    },
    price:{
        type:Number,
        default:0
    },
    subTotal:{
        type:Number,
        default: 0
    }
},
    { timestamps:true}
    )
    


const CartSchema = new mongoose.Schema({

    
            userId:{
                type:String,
                
            },
            products:[cartItems],
            totalPrice:{
                type:Number,
                default: 0
            }
            
       
})

module.exports = mongoose.model("Cart", CartSchema);