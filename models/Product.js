const mongoose = require('mongoose');

const Order = new mongoose.Schema({
    
        orderId:{
            type:String
        },
        quantity:{
            type:Number    
        }
    
})
const Rating = new mongoose.Schema({
    
        userId:{
            type:String
        },
        rate:{
            type:Number    
        }
    
})

const productSchema = new mongoose.Schema({
    image:{
        type: String
    },
    name:{
        type:String 
    },
    description:{
        type:String 
    },
    brand:{
        type: String
    },
    category:{
        type: String
    },

    price:{
        type:Number
    },

    quantity:{
        type:Number
    },
    isActive:{
        type:Boolean,
        default:true
    },
    createOn:{
        type: Date,
        default: new Date()
    },
    orders:[Order],
    ratings:[Rating]
    


})


module.exports = mongoose.model("Products", productSchema);