const mongoose = require('mongoose');

const Products = new mongoose.Schema({
    
        productId:{
            type:String
        },
        quantity:{
            type:Number
        }
    
})

const OrderSchema = new mongoose.Schema({

    
            userId:{
                type:String,
                
            },
            products:[Products],
            totalAmount:{
                type:Number
            },
            purchasedOn:{
                    type: Date,
                    default: new Date()
                }
       
})

module.exports = mongoose.model("Order", OrderSchema);