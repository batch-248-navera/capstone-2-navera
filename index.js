require('dotenv').config();

const express = require('express');
const mongoose = require('mongoose');
const PORT = process.env.PORT||4000;
const cors = require("cors");
const app = express();

app.use(cors());
app.use(express.json({limit: '25mb'}));
app.use(express.urlencoded({extended:true},{limit: '25mb'}));


mongoose.set('strictQuery',true);

mongoose.connect(process.env.DB_URI,
    {
        useNewUrlParser:true,
        useUnifiedTopology: true
    })

let db = mongoose.connection;

db.on("error",console.error.bind(console,"Error connectin to the database!"));
db.once("open",()=>console.log("MongoDB connection is successful!"));

const userRoutes = require("./routes/userRoutes");
app.use("/users",userRoutes);

const productRoutes = require("./routes/productRoutes");
app.use("/products",productRoutes);

const orderRoutes = require("./routes/orderRoutes");
app.use("/orders",orderRoutes);

const cartRoutes = require("./routes/cartRoutes");
app.use("/cart",cartRoutes);


app.listen(PORT, ()=>{
    
    console.log(`Server is now online on port ${PORT}`);

})